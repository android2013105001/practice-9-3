package resolution.example6.zzeulki.practice93;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends Activity implements Runnable{

    private MediaPlayer mPlayer = null;
    private ProgressBar mProgress;
    private SeekBar mSeekbar;

    public void showProgress(){
        if(mPlayer != null && mPlayer.isPlaying()){
            mProgress.setProgress(mPlayer.getCurrentPosition());

            //Log.i("progress",""+progress);
            //mProgress.setProgress(progress);
            Log.i("Mprogress",""+mProgress.getProgress());
            ///mPlayer.seekTo(progress);
            Log.i("Mplayer",""+mPlayer.getCurrentPosition());


            TextView output = (TextView)findViewById(R.id.textview1);
            output.setText("재생 중" + "\n현재 위치: " + mPlayer.getCurrentPosition()/1000
            +"\n전체 길이: "+mPlayer.getDuration()/1000);

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initMP();
        mSeekbar.setOnSeekBarChangeListener(new SeekBarChangeListener());
    }

    @Override
    protected void onDestroy(){
        if(mPlayer != null){
            mPlayer.release();
            mPlayer = null;
        }
        super.onDestroy();
    }

    public void initMP(){
        mPlayer = MediaPlayer.create(this,R.raw.music);

        mProgress = (ProgressBar) findViewById(R.id.progressbar1);
        mProgress.setProgress(0);
        mProgress.setMax(0);

        mSeekbar = (SeekBar) findViewById(R.id.seekbar1);


        ProTread thrd = new ProTread();
        thrd.setDaemon(true);
        thrd.start();
    }


    public void startMP(View v){

        Button btn;
        btn = (Button) findViewById(R.id.button);
        btn.setEnabled(false);
        btn = (Button) findViewById(R.id.button2);
        btn.setEnabled(true);

        mProgress.setProgress(0);
        mProgress.setMax(mPlayer.getDuration());

        mPlayer.start();
        mPlayer.setLooping(true);

        mSeekbar.setProgress(0);
    }

    public void pauseMP(View v){
        if(mPlayer.isPlaying()){
            mPlayer.pause();
            TextView output = (TextView) findViewById(R.id.textview1);
            output.setText("일시정지");
        }
        else
            mPlayer.start();
    }


    public void stopMP(View v){
        mPlayer.stop();
        initMP();

        Button btn;
        btn = (Button) findViewById(R.id.button);
        btn.setEnabled(true);
        btn = (Button) findViewById(R.id.button2);
        btn.setEnabled(false);

        mProgress.setProgress(0);
        mProgress.setMax(0);

        mSeekbar.setProgress(0);

        TextView output = (TextView) findViewById(R.id.textview1);
        output.setText("중지");
    }

    @Override
    public void run() {

    }


    class ProTread extends Thread{
        public Handler mHandler = new Handler(){
            public void handleMessage(Message msg){
                if(msg.what == 1){
                    showProgress();
                }

            }
        };

        public void run(){
            while(mPlayer != null){
                try{
                    Thread.sleep(500);
                }catch (InterruptedException e){
                    e.printStackTrace();
                    break;
                }
                mHandler.sendMessage(Message.obtain(mHandler,1));
            }
        }
    }

    private class SeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

        public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {

            //Log.i("---progress",""+progress);
            mProgress.setProgress(progress*1000);
            //Log.i("Mprogress",""+mProgress.getProgress());
            mPlayer.seekTo(progress*1000);
            //Log.i("Mplayer",""+mPlayer.getCurrentPosition());

            //TextView output = (TextView)findViewById(R.id.textview1);
            //output.setText("재생 중" + "\n현재 위치: " + mPlayer.getCurrentPosition()/1000
            //        +"\n전체 길이: "+mPlayer.getDuration()/1000);
        }

        public void onStartTrackingTouch(SeekBar arg0) {


        }

        public void onStopTrackingTouch(SeekBar arg0) {
            mPlayer.start();
        }

    }
}



